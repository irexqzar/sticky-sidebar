"use strict";
// document.addEventListener("DOMContentLoaded", function () {
class Sticky {
    constructor(options) {
        this.main = document.querySelector(options.main);
        this.mainLeft = document.querySelector(options.mainLeft);
        this.mainRight = document.querySelector(options.mainRight);
        this.sticky = document.querySelector(options.sticky);

        this.maxWidthScreen = options.maxWidthScreen;
        this.paddingTopSticky = options.paddingTopSticky;
        this.paddingBottomSticky = options.paddingBottomSticky;
        this.specialPaddingForSticky = options.specialPaddingForSticky;

        this.size = {
            stopPoint: null,
            screen: {
                height: null, // на начальном этапе
            },
            mainLeft: {
                height: null,
            },
            mainRight: {
                height: null,
                top: null,
                bottom: null,
            },
            sticky: {
                height: null, // на начальном этапе
                bottom: null,
            },
        };

        this.access = {
            stickyHeight: false,
            screeWidth: false,
            resize: true,
            blocksHeight: false,
        };

        this.start();
    }

    start() {
        //проверка на размер блока и возможность прилипания
        this.size.sticky.height = this.sticky.offsetHeight;
        this.size.screen.height = document.body.offsetHeight;
        if (this.size.sticky.height + 50 < this.size.screen.height) {
            this.access.stickyHeight = true;
        }

        if (document.documentElement.clientWidth > this.maxWidthScreen) {
            this.access.screeWidth = true;
        }

        if (this.mainLeft.offsetHeight > this.sticky.offsetHeight) {
            this.access.blocksHeight = true;
        }

        if (this.access.screeWidth && this.access.stickyHeight && this.access.blocksHeight) {

            window.addEventListener('resize', (e) => {
                if (document.documentElement.clientWidth <= this.maxWidthScreen) {
                this.cleanSticky();
                // console.log('мало');
                this.access.resize = false;
            }
            if (document.documentElement.clientWidth> this.maxWidthScreen) {
                // console.log('много');
                this.access.resize = true;
            }
        });

            document.addEventListener('scroll', (e) => {

                if (this.access.resize){
                this.equateHeight();
                this.findCoordinates();
                this.replaceSticky();
            }


        });
        }

    }

    cleanSticky(){
        this.mainRight.style.height = `auto`;
        this.sticky.style.position = 'relative';
        this.sticky.style.top = 'auto';
        this.sticky.style.bottom = 'auto';
    };

    replaceSticky() {

        if (this.size.mainRight.top < this.paddingTopSticky && this.size.mainRight.bottom - this.specialPaddingForSticky > this.size.sticky.bottom) {
            // console.log('поехали');
            this.sticky.style.position = 'fixed';
            this.sticky.style.top = `${this.paddingTopSticky}px`;
            this.sticky.style.bottom = `auto`;

        } else {

            if (this.size.mainRight.bottom - this.specialPaddingForSticky <= this.size.sticky.bottom) {
                // console.log('оставь внизу');
                this.sticky.style.position = 'absolute';
                this.sticky.style.bottom = `${this.specialPaddingForSticky}px`;
                this.sticky.style.top = 'auto';
                this.size.stopPoint = this.sticky.getBoundingClientRect().top + pageYOffset
            }

            if (this.size.mainRight.top > this.paddingTopSticky) {
                // console.log('оставь вверху');
                this.sticky.style.position = 'relative';
                this.sticky.style.top = '0px';
                return;
            }

            if (this.size.stopPoint > pageYOffset) {
                // console.log('поехали2');
                this.sticky.style.position = 'fixed';
                this.sticky.style.top = `${this.paddingTopSticky}px`;
                this.sticky.style.bottom = `auto`;
            }
        }

    }

    findCoordinates() {
        this.size.mainRight.top = this.mainRight.getBoundingClientRect().top;
        this.size.mainRight.bottom = this.mainRight.getBoundingClientRect().bottom;
        this.size.sticky.bottom = this.sticky.getBoundingClientRect().bottom;
    }

    equateHeight() {
        this.mainRight.style.height = `${this.mainLeft.offsetHeight}px`;
    }
}

window.sticky = Sticky;

// let sticky = new Sticky({
//     main: `#main`,
//     mainLeft: `#mainLeft`,
//     mainRight: `#mainRight`,
//     sticky: `#sticky`,
//     maxWidthScreen: `1023`,
//     paddingTopSticky: `25`,
//     paddingBottomSticky: `25`,
//     specialPaddingForSticky: `100`, // если нужно остановить чуть раньше чем низ документа
// });

// });

